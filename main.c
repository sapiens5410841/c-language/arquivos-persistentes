#include <stdio.h>

// Defini��o de uma estrutura para armazenar informa��es
struct Informacoes {
    int idade;
    float altura;
};

int main() {
    // Vari�vel para armazenar as informa��es
    struct Informacoes dados;
    // Vari�vel para indicar se os dados foram carregados do arquivo
    int dados_carregados = 0;

    int opcao;
    do {
        // Exibir o menu
        printf("\nMenu:\n");
        printf("1 - Criar nova idade e altura\n");
        printf("2 - Carregar idade e altura\n");
        printf("3 - Exibir idade e altura\n");
        printf("0 - Sair\n");
        printf("Escolha uma opcao: ");
        scanf("%d", &opcao);

        switch (opcao) {
            case 1:
                // Criar nova idade e altura
                printf("Informe a idade: ");
                scanf("%d", &dados.idade);
                printf("Informe a altura: ");
                scanf("%f", &dados.altura);
                // Abrir o arquivo para escrita
                FILE *arquivo = fopen("dados.txt", "wb");
                // Verificar se o arquivo foi aberto com sucesso
                if (arquivo == NULL) {
                    printf("Erro ao abrir o arquivo.\n");
                    return 1;
                }
                // Escrever os dados no arquivo
                fwrite(&dados, sizeof(struct Informacoes), 1, arquivo);
                // Fechar o arquivo
                fclose(arquivo);
                dados_carregados = 1;
                printf("Idade e altura criadas com sucesso.\n");
                break;
            case 2:
                // Carregar idade e altura
                arquivo = fopen("dados.txt", "rb");
                // Verificar se o arquivo foi aberto com sucesso
                if (arquivo == NULL) {
                    printf("Erro ao abrir o arquivo. Idade e altura nao informadas.\n");
                    dados_carregados = 0;
                } else {
                    // Ler os dados do arquivo
                    fread(&dados, sizeof(struct Informacoes), 1, arquivo);
                    // Fechar o arquivo
                    fclose(arquivo);
                    dados_carregados = 1;
                    printf("Idade e altura carregadas com sucesso.\n");
                }
                break;
            case 3:
                // Exibir idade e altura
                if (dados_carregados) {
                    printf("Idade: %d\n", dados.idade);
                    printf("Altura: %.2f\n", dados.altura);
                } else {
                    printf("Idade e altura nao informadas.\n");
                }
                break;
            case 0:
                // Sair do programa
                printf("Saindo do programa.\n");
                break;
            default:
                printf("Opcao invalida. Tente novamente.\n");
                break;
        }
    } while (opcao != 0);

    return 0;
}
